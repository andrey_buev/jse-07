package ru.buevas.tm.dao;

import java.util.ArrayList;
import java.util.List;
import ru.buevas.tm.entity.Project;

public class ProjectDao {

    private List<Project> projects = new ArrayList<>();

    public Project create(final String name) {
        final Project newProject = new Project(name);
        projects.add(newProject);
        return newProject;
    }

    public void clear() {
        projects.clear();
    }

    public List<Project> findAll() {
        return projects;
    }
}
