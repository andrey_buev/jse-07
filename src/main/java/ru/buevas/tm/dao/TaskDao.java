package ru.buevas.tm.dao;

import java.util.ArrayList;
import java.util.List;
import ru.buevas.tm.entity.Task;

public class TaskDao {

    private List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        final Task newTask = new Task(name);
        tasks.add(newTask);
        return newTask;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }
}
