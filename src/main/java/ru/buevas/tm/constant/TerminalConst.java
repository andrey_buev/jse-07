package ru.buevas.tm.constant;

/**
 * Константы для консольной версии приложения приложения
 *
 * @author Andrey Buev
 */
public class TerminalConst {

    /**
     * Допустимые команды консольного приложения
     */
    public static class CmdParams {

        public static final String VERSION = "version";
        public static final String ABOUT = "about";
        public static final String HELP = "help";
        public static final String EXIT = "exit";

        public static final String PROJECT_CREATE = "project-create";
        public static final String PROJECT_LIST = "project-list";
        public static final String PROJECT_CLEAR = "project-clear";

        public static final String TASK_CREATE = "task-create";
        public static final String TASK_LIST = "task-list";
        public static final String TASK_CLEAR = "task-clear";
    }
}
