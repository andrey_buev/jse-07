package ru.buevas.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import ru.buevas.tm.constant.TerminalConst.CmdParams;
import ru.buevas.tm.dao.ProjectDao;
import ru.buevas.tm.dao.TaskDao;
import ru.buevas.tm.entity.Project;
import ru.buevas.tm.entity.Task;

/**
 * Основной класс приложения
 *
 * @author Andrey Buev
 */
public class App {

    private static final ProjectDao projectDao = new ProjectDao();

    private static final TaskDao taskDao = new TaskDao();

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


    /**
     * Точка входа в приложение
     *
     * @param args - параметры командной строки, переданные при старте приложения
     */
    public static void main(String[] args) {
        printWelcome();
        run(args);
        process();
    }

    /**
     * Запуск приложения с аргументом из командной строки
     *
     * @param args - массив аргументов
     */
    public static void run(final String[] args) {
        if (args == null || args.length < 1) {
            return;
        }

        final String command = args[0];
        System.exit(execCommand(command));
    }

    /**
     * Запуск приложения в режиме бесконечного цикла
     */
    public static void process() {
        String command = "";
        while (!CmdParams.EXIT.equals(command)) {
            try {
                command = reader.readLine();
                execCommand(command);
                System.out.println();
            } catch (IOException e) {
                printError(e.getMessage());
            }
        }
    }

    /**
     * Выполнение команды
     *
     * @param command - выполняемая команда
     * @return код ошибки или 0 в случае успешного завершения
     */
    public static int execCommand(final String command) {
        if (command == null || command.isEmpty()) {
            return -1;
        }
        switch (command) {
            case CmdParams.VERSION: {
                return printVersion();
            }
            case CmdParams.ABOUT: {
                return printAbout();
            }
            case CmdParams.HELP: {
                return printHelp();
            }
            case CmdParams.EXIT: {
                return exit();
            }
            case CmdParams.PROJECT_CREATE: {
                return createProject();
            }
            case CmdParams.PROJECT_LIST: {
                return listProject();
            }
            case CmdParams.PROJECT_CLEAR: {
                return clearProject();
            }
            case CmdParams.TASK_CREATE: {
                return createTask();
            }
            case CmdParams.TASK_LIST: {
                return listTask();
            }
            case CmdParams.TASK_CLEAR: {
                return clearTask();
            }
            default: {
                return printError("Unknown parameter");
            }
        }
    }

    /**
     * Вывод на экран приветствия
     */
    public static void printWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Обработка команды вывода версии приложения
     *
     * @return 0
     */
    public static int printVersion() {
        System.out.println("Task Manager version 1.0.0");
        return 0;
    }

    /**
     * Обработка команды вывода сведений о разработчике
     *
     * @return 0
     */
    public static int printAbout() {
        System.out.println("Developer: Andrey Buev (buev_as@mail.ru)");
        return 0;
    }

    /**
     * Обработка команды вывода справки
     *
     * @return 0
     */
    public static int printHelp() {
        System.out.println(String.format("%s - Display version information", CmdParams.VERSION));
        System.out.println(String.format("%s - Display version information", CmdParams.ABOUT));
        System.out.println(String.format("%s - Display version information", CmdParams.HELP));
        System.out.println(String.format("%s - Display version information", CmdParams.EXIT));
        System.out.println();
        System.out.println(String.format("%s - Create new project", CmdParams.PROJECT_CREATE));
        System.out.println(String.format("%s - Display list of projects", CmdParams.PROJECT_LIST));
        System.out.println(String.format("%s - Clear list of projects", CmdParams.PROJECT_CLEAR));
        System.out.println();
        System.out.println(String.format("%s - Create new task", CmdParams.TASK_CREATE));
        System.out.println(String.format("%s - Display list of tasks", CmdParams.TASK_LIST));
        System.out.println(String.format("%s - Clear list of tasks", CmdParams.TASK_CLEAR));
        return 0;
    }

    /**
     * Обработка команды создания проекта
     */
    public static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        try {
            final String newProjectName = reader.readLine();
            projectDao.create(newProjectName);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды вывода списка проектов
     */
    public static int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println("Available projects:");
        List<Project> projects = projectDao.findAll();
        if (projects.isEmpty()) {
            System.out.println("Empty");
        } else {
            projects.forEach((project) -> System.out.println(project.toString()));
        }
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Обработка команды отчистки списка проектов
     */
    public static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectDao.clear();
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Обработка команды создания задачи
     */
    public static int createTask() {
        System.out.println("[CREATE TASK]");
        try {
            final String newTaskName = reader.readLine();
            taskDao.create(newTaskName);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            return printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды вывода списка задач
     */
    public static int listTask() {
        System.out.println("[LIST TASK]");
        System.out.println("Available tasks:");
        List<Task> projects = taskDao.findAll();
        if (projects.isEmpty()) {
            System.out.println("Empty");
        } else {
            projects.stream().forEach((task) -> System.out.println(task.toString()));
        }
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Обработка команды отчистки списка задач
     */
    public static int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskDao.clear();
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Вывод на экран сообщения об ошибке
     *
     * @param message - сообщение
     * @return -1
     */
    public static int printError(final String message) {
        System.out.println("[ERROR]");
        System.out.println(message);
        return -1;
    }

    /**
     * Завершение работы приложения
     *
     * @return 0
     */
    public static int exit() {
        System.exit(0);
        return 0;
    }
}
